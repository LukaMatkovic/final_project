<?php   
        include 'includes/config.php';
        include 'includes/header.php';
        include 'includes/db.php';
?>
<section class="pricelist-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Our services</h2>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script>

$(document).ready(function() {
    $('#example').DataTable();
});

</script>

        <div class="container">
            <table id="example" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <th>Service Name</th>
                    <th>Service Price</th>
                    <th>Service Duration</th>
                    <th>Service Description</th>
                </thead>
                <tbody>

        <?php
            $sql = "SELECT * FROM services";
            $result = mysqli_query($connection, $sql);

            while ($row = mysqli_fetch_assoc($result)) {
                $name = $row['service_name'];
                $description = $row['service_description'];
                $price = $row['service_price(RSD)'];
                $duration = $row['service_duration(hrs)'];
                $photo = $row['service_photo'];

                echo "<tr>
                        <td>{$name}</td>
                        <td>{$price} RSD</td>
                        <td>{$duration} hours</td>
                        <td>{$description}</td>
                    </tr>";
            }
        ?>
                </tbody>
            </table>
        </div>
<?php    include 'includes/footer.php'; ?>
