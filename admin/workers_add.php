<?php
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add new worker</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container">
        <div class="row">
            <div id="formWindow" class="client">
                <form action="workers_add_check.php" method="POST" id="form_workers" enctype="multipart/form-data">

                    <label for="firstname">Firstname</label><br>
                    <input type="text" name="firstname" id="firstname" placeholder="Firstname" required><br>

                    <label for="lastname">Lastname</label><br>
                    <input type="text" name="lastname" id="lastname" placeholder="Lastname" required><br>

                    <label for="email">E-Mail</label><br>
                    <input type="email" name="email" id="email" placeholder="email@mail.com" required><br>

                    <label for="phone">Phone Number</label><br>
                    <input type="text" name="phone" id="phone" placeholder="+381xxxxxxx" required><br>

                    <label for="about">Description</label><br>
                    <textarea name="about" id="about" cols="30" rows="10"></textarea><br>

                    <label for="file">Image</label><br>
                    <input type="file" name="photo"><br>

                    <input type="submit" name="submit" value="Add Worker" class="btn btn-primary btn-block">

                </form>
            </div>
        </div>
    </div>
</main>
<?php include '../includes/footer.php'; ?>