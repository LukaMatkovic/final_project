<?php
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
?>
        <section class="admin-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>Welcome, <strong><?php echo $_SESSION['user_firstname']." ".$_SESSION['user_lastname'];?></strong>!</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="adminoptions">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="problem_review.php" class="btn btn-primary btn-block">New Requests</a>
                    </div>
                    <div class="col-md-2">
                        <a href="problems.php" class="btn btn-primary btn-block">In Process</a>
                    </div>            
                    <div class="col-md-2">
                        <a href="services_update.php" class="btn btn-primary btn-block">Update Services</a>
                    </div>            
                    <div class="col-md-2">
                        <a href="workers_update.php" class="btn btn-primary btn-block">Update Workers</a>
                    </div>            
                    <div class="col-md-2">
                        <a href="archive.php" class="btn btn-primary btn-block">Archive</a>
                    </div>
                    <div class="col-md-2">
                        <a href="schedule.php" class="btn btn-primary btn-block">Schedule</a>
                    </div>
                </div>
            </div>
        </section>
        <main id="start">
        <div class="home-inner">
                <div class="container startcont">
            <?php
            $sql = "SELECT * FROM problems WHERE problem_status=1";
            $result = mysqli_query($connection, $sql) or die('Query 1 failed: '.mysqli_error($connection));
            $requests = mysqli_num_rows($result);

            if ($requests>1) {
                echo "You have <a href='problem_review.php'><strong>{$requests} new requests</strong></a> waiting to be approved!";
            }
            elseif ($requests == 1) {
                echo "You have <a href='problem_review.php'><strong>a new request</strong></a>
                waiting to be approved!";
            }
            else {
                echo "You have no new requests.";
            }
            ?>

        </main>
<?php include '../includes/footer.php'; ?>