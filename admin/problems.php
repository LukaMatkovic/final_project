<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Cars currently in our mechanic-service</h2>
            </div>
        </div>
    </div>
</section>
<main>

    <section class="searchbar text-center">
        <form action="" method="POST" id="search">
            <label for="search">Search specific cases by inserting Carplate, Firstname, Lastname, Request No. OR Day of the first visit (yyyy-mm-dd)</label><br>
            <input type="text" name="search">
            <input type="submit" class="btn btn-info" value="Search">
        </form>

    </section>

<?php
$where = "";
$limit = "";

$page = 1;
$start = 0;
$show = 3;
$count = 1;

$sql = "SELECT problems.*, carstatus.carstatus_name, users.*, cars.*, carbrands.*, reservations.*
        FROM problems 
        INNER JOIN  carstatus ON problems.problem_status = carstatus_id 
        INNER JOIN  users ON problems.problem_user = users.user_id 
        INNER JOIN  cars ON problems.problem_car = cars.car_id
        INNER JOIN  reservations ON problems.problem_reservation = reservations.reservation_id
        INNER JOIN  carbrands ON cars.car_brand = carbrands.brand_id";
$where = " WHERE problem_status BETWEEN 2 AND 5";
$order = " ORDER BY reservations.report_time DESC";

//if is written something in searchbar, add that input to query
if(isset($_POST['search']) && !empty($_POST['search'])) {
    $search = test_input($_POST['search']);
    $where.=" AND (cars.car_plate LIKE '%$search%' OR users.user_email LIKE '%$search%' OR users.user_firstname LIKE '%$search%' OR users.user_lastname LIKE '%$search%' OR problems.problem_id LIKE '%$search%' OR reservations.reservation_day LIKE '%$search%')";
    $sql.=$where;
    $sql.=$order;

}
//if not, search all, but add pagination
else {
    $sql.=$where;
    $sql_count = $sql;
    $result_count = mysqli_query($connection, $sql_count) or die('Query 1 failed: '.mysqli_error($connection));
    $count = mysqli_num_rows($result_count);
    $count = ceil($count/$show);
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    else {
        $page = 1;
    }
    $start =$page*$show-$show;
    $limit = " LIMIT $start, $show";
    $sql.=$order;
    $sql.=$limit;
    }

//show and use data from the database
$result = mysqli_query($connection, $sql) or die('Query 1 failed: '.mysqli_error($connection));
while ($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {

    $problem_id = $row['problem_id'];
    $user_id = $row['problem_user'];
    $car_id = $row['problem_car'];
    $car_statusname = $row['carstatus_name'];
    $reservation_id = $row['problem_reservation'];
    $problem_status = $row['problem_status'];
    $comment_user = $row['comment_user'];
    $comment_worker = $row['comment_worker'];
    $totalprice = $row['totalprice'];
    $cd = ($problem_id*3+5)*71;


    $user_firstname = $row['user_firstname'];
    $user_lastname = $row['user_lastname'];
    $user_email = $row['user_email'];
    $user_phone = $row['user_phone'];
    $user_token = $row['user_token'];

    $car_plate = $row['car_plate'];
    $car_brand = $row['brand_name'];
    $car_name = $row['car_name'];

    $reservation_day = $row['reservation_day'];
    $reservation_hour = $row['reservation_hour'];
    $duration = $row['duration'];

    $starttime = substr($reservation_hour, 0, 5);
    $enddatetime = strtotime($reservation_day." ".$reservation_hour)+$duration*60*60;
    $end = date('Y-m-d H:i', $enddatetime);
    $endtime = date('H:i', $enddatetime);


?>

    <div class="container">
        <div class="row">
            <div class="client" id="<?php echo $problem_id; ?>">
                <h2>Request No. <?php echo $problem_id; ?></h2>
                <p><strong>Client: </strong><?php echo $user_firstname." ".$user_lastname; ?></p>
                <p><strong>Reservation: </strong><?php echo $reservation_day; ?>, <?php echo $starttime; ?>-<?php echo $endtime; ?></p>
                <p><strong>Client email: </strong><?php echo $user_email; ?></p>
                <p><strong>Client phone: </strong><?php echo $user_phone; ?></p>
                <p><strong>Car plate: </strong><?php echo $car_plate; ?></p>
                <p><strong>Car model: </strong><?php echo $car_brand." ".$car_name; ?></p>
                <p><strong>User problem description: </strong><?php echo $comment_user; ?></p>
                <p><strong>Services: </strong></p>
                <table class='table table-striped table-bordered table-hover'>
                    <thead>
                        <th>Service</th>
                        <th>Your Comment</th>
                        <th>Price (RSD)</th>
                        <th class="info">UPDATE</th>
                        <th class="danger">DELETE</th> 
                    </thead>
                    <tbody>

        <?php

        $sql_services = "SELECT problem_services.ps_id, services.service_name, problem_services.worker_comment, problem_services.service_finalprice FROM problem_services  INNER JOIN services ON problem_services.service_id=services.service_id WHERE problem_services.problem_id=$problem_id ";
        $result_services = mysqli_query($connection, $sql_services) or die('Query 4 failed: '.mysqli_error($connection));

        while ($row=mysqli_fetch_array($result_services, MYSQLI_BOTH)) {
            $sid = $row['ps_id'];
            $delid = $sid*12345;
            $code = ($sid*13-15)*17+19;
            $service_name = $row['service_name'];
            $service_comment = $row['worker_comment'];
            $service_price = $row['service_finalprice'];
    ?>

                        <tr>
                            <td><?php echo $service_name; ?></td>
                            <td><?php echo $service_comment; ?></td>
                            <td><?php echo $service_price; ?></td>
                            <td><a class="btn btn-info btn-block" href='service_update.php?cd=<?php echo $code; ?>&tk=<?php echo $user_token; ?>'>Update</a></td>
                            <td><a id="<?php echo $delid; ?>" class="btn btn-danger btn-block" href='service_delete.php?cd=<?php echo $code; ?>&tk=<?php echo $user_token; ?>'>Delete</a></td>
                        </tr>
                        <script>
                            document.getElementById("<?php echo $delid; ?>").addEventListener("click", function(event) {
                                event.preventDefault();
                                var choice = confirm("Are you sure you want to delete this service?");
                                if (choice) {
                                    window.location.href = this.getAttribute('href');
                                }
                                else {
                                    return false;
                                }
                            });    
                        </script>
    <?php
        }
    ?>
                    </tbody>
                </table>
                <p><strong>Current car status: </strong><?php echo $car_statusname; ?></p>
                <p><strong>Your comment: </strong><?php echo $comment_worker; ?></p>
                <p><strong>Final price: </strong><?php echo $totalprice; ?> RSD</p>
                <a class="btn btn-primary btn-block"  href='service_add.php?cd=<?php echo $cd; ?>'>Add new service that needs to be done</a><br>
                <a class="btn btn-info btn-block"  href='problem_update.php?cd=<?php echo $cd; ?>'>Update process</a><br>
                <a class="btn btn-success btn-block" href='problem_sendemail.php?cd=<?php echo $cd; ?>'>Send email</a>
            </div>
        </div>
    </div>

<?php

}
?>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12 text-center">
                <ul class="pagination">
            <?php
            if($count!=1){
                for ($i = 1; $i<=$count; $i++) {
                    if ($i==$page) {
                        echo '<li class="page-item"><a class="page-link pagactive" href="problems.php?page='.$i.'">'.$i.'</a></li>';
                    }
                    else {
                        echo '<li class="page-item"><a class="page-link" href="problems.php?page='.$i.'">'.$i.'</a></li>';
                    }
                }}

            ?>
                </ul>
            </div>
        </div>
    </div>
</main>    


<?php include '../includes/footer.php'; ?>