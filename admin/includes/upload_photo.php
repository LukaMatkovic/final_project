<?php
function checkPhoto($name, $picture) {
        global $photo;

        $name = htmlspecialchars_decode($name);
        $name = str_replace(' ', '_', $name);

        $file_name = $_FILES['photo']["name"];
        $file_temp = $_FILES['photo']["tmp_name"];
        $file_size = $_FILES['photo']["size"];
        $file_type = $_FILES['photo']["type"];
        $file_error = $_FILES['photo']["error"];

    if($file_error > 0) {
        echo "Photo has not been uploaded!";
    }
    else {
        if(!exif_imagetype($file_temp)) {
            exit("File is not a picture!");
        }

        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);

        $newfilename = $name.".$extension";
        $directory = "../images/".$picture;
        $photo = "$directory/$newfilename";

        if(!is_dir($directory)) {
            mkdir($directory);
        }

        if(move_uploaded_file($file_temp,$photo)) {

        }
        else {
            echo "Error :(";
        }
    }
    return $photo;
    }
?>