<?php
if(!defined("SECRET")){
    die();
}
?>
<?php

$sql = "SELECT problems.*, carstatus.carstatus_name, users.*, cars.*, carbrands.*, reservations.*
        FROM problems 
        INNER JOIN  carstatus ON problems.problem_status = carstatus_id 
        INNER JOIN  users ON problems.problem_user = users.user_id 
        INNER JOIN  cars ON problems.problem_car = cars.car_id
        INNER JOIN  reservations ON problems.problem_reservation = reservations.reservation_id
        INNER JOIN  carbrands ON cars.car_brand = carbrands.brand_id";
$order = " ORDER BY reservations.report_time DESC";
$sql.=$where;
$sql.=$order;
$result = mysqli_query($connection, $sql) or die('Query 1 failed: '.mysqli_error($connection));

while ($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {

    $problem_id = $row['problem_id'];
    $user_id = $row['problem_user'];
    $car_id = $row['problem_car'];
    $car_statusname = $row['carstatus_name'];
    $reservation_id = $row['problem_reservation'];
    $problem_status = $row['problem_status'];
    $comment_user = $row['comment_user'];
    $comment_worker = $row['comment_worker'];
    $totalprice = $row['totalprice'];
    $cd = ($problem_id*3+5)*7;

    $user_firstname = $row['user_firstname'];
    $user_lastname = $row['user_lastname'];
    $user_email = $row['user_email'];
    $user_phone = $row['user_phone'];
    $user_token = $row['user_token'];

    $car_plate = $row['car_plate'];
    $car_brand = $row['brand_name'];
    $car_name = $row['car_name'];

    $reservation_day = $row['reservation_day'];
    $reservation_hour = $row['reservation_hour'];
    $duration = $row['duration'];

    $starttime = substr($reservation_hour, 0, 5);
    $enddatetime = strtotime($reservation_day." ".$reservation_hour)+$duration*60*60;
    $end = date('Y-m-d H:i', $enddatetime);
    $endtime = date('H:i', $enddatetime);



?>
    <div class="container">
        <div class="row">
            <div class="client" id="<?php echo $cd; ?>">
            <h2>Request No. <?php echo $problem_id; ?></h2>
            <p><strong>Client: </strong><?php echo $user_firstname." ".$user_lastname; ?></p>
            <p><strong>Reservation: </strong><?php echo $reservation_day; ?>, <?php echo $starttime; ?>-<?php echo $endtime; ?></p>
            <p><strong>Client email: </strong><?php echo $user_email; ?></p>
            <p><strong>Client phone: </strong><?php echo $user_phone; ?></p>
            <p><strong>Car plate: </strong><?php echo $car_plate; ?></p>
            <p><strong>Car model: </strong><?php echo $car_brand." ".$car_name; ?></p>
            <p><strong>User problem description: </strong><?php echo $comment_user; ?></p>
            <p><strong>Your final comment: </strong><?php echo $comment_worker; ?></p>
            <p><strong>Services needed: </strong></p>
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <th>Service</th>
                    <th>Price</th>
                    <th>Time</th>
                </thead>
                <tbody>      

<?php

    $sql_services = "SELECT services.service_name, problem_services.service_finalprice, problem_services.service_finaltime FROM problem_services  INNER JOIN services ON problem_services.service_id=services.service_id WHERE problem_services.problem_id=$problem_id ";
    $result_services = mysqli_query($connection, $sql_services) or die('Query 4 failed: '.mysqli_error($connection));

    while ($row=mysqli_fetch_array($result_services, MYSQLI_BOTH)) {
        $service_name = $row['service_name'];
        $service_price = $row['service_finalprice'];
        $service_time = $row['service_finaltime'];
?>

                    <tr>
                        <td><?php echo $service_name; ?></td>
                        <td><?php echo $service_price; ?></td>
                        <td><?php echo $service_time; ?></td>
                    </tr>
<?php
    }

?>
                </tbody>
            </table>
            <?php if ($problem_status==6){
                echo "<p><strong>Final price: </strong>".$totalprice." RSD</p>";
            }
            ?>
            </div>
        </div>
    </div>

<?php
}
?>
           