<?php  
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add new service</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container">
        <div class="row">

            <div id="formWindow" class="client">
                <form action="services_add_check.php" method="POST" id="form_services" enctype="multipart/form-data">

                    <label for="name">Service name</label><br>
                    <input type="text" name="name" id="name" placeholder="Enter service name here" value="" required><br>

                    <label for="description">Description</label><br>
                    <textarea name="description" id="description" cols="30" rows="10" value=""></textarea><br>

                    <label for="price">Price (RSD)</label><br>
                    <input type="number" name="price" id="price" value="" required><br>

                    <label for="duration">Duration (hours)</label><br>
                    <input type="number" min="0.5" max="4" step="0.5"  name="duration" id="duration" value="" required><br>

                    <label for="file">Photo</label><br>
                    <input type="file" name="photo"><br>

                    <input type="submit" name="submit" value="Add Service" class="btn btn-primary btn-block">

                </form>
            </div>
        </div>
    </div>
</main>

<?php include '../includes/footer.php'; ?>