<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';

?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Update info about selected service</h2>
            </div>
        </div>
    </div>
</section>
<?php

    $id = 0;
    if(isset($_GET['id'])) { $id = test_input($_GET['id']);}

    $sql_services = "SELECT * FROM services  WHERE service_id=$id ";
    $result_services = mysqli_query($connection, $sql_services) or die('Query failed: '.mysqli_error($connection));

    while ($row=mysqli_fetch_array($result_services, MYSQLI_BOTH)) {
        $name = $row['service_name'];
        $desciption = $row['service_description'];
        $price = $row['service_price(RSD)'];
        $duration = $row['service_duration(hrs)'];
    }
    ?>
    <main>
    <div class="container">
        <div class="row">
            <div class="client">

    <form action="services_change_check.php" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="id" value='<?php echo $id;?>' readonly><br>

        <label for="name">Service name</label><br>
        <input type="text" name="name" id="name" value="<?php echo $name; ?>"><br>

        <label for="description">Description</label><br>
        <textarea name="description" id="description" cols="30" rows="10"><?php echo $desciption; ?></textarea><br>

        <label for="price">Price (RSD)</label><br>
        <input type="number" name="price" id="price" value="<?php echo $price; ?>"><br>

        <label for="duration">Duration (hours)</label><br>
        <input type="number" min="0.5" max="4" step="0.5" name="duration" id="duration" value="<?php echo $duration; ?>"><br>

        <label for="file">Photo</label><br>
        <input type="file" name="photo"><br>
        
        <input type="submit" value="Change Service Data" class="btn btn-success btn-block">
    </form>
</div>
</div>
</main>



<?php include '../includes/footer.php'; ?>