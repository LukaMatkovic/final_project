<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Update this case</h2>
            </div>
        </div>
    </div>
</section>
<?php

    $problem_id = 0;
    if(isset($_GET['cd'])) { $problem_id = (test_input($_GET['cd'])/71-5)/3;}

    $sql = "SELECT problems.problem_status, carstatus.carstatus_name, problems.comment_worker, problems.totalprice FROM problems  INNER JOIN carstatus ON problems.problem_status = carstatus.carstatus_id WHERE problems.problem_id = $problem_id ";
    $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));

    while ($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {
        
        $problemstatus = $row['problem_status'];
        $carstatusname = $row['carstatus_name'];
        $comment = $row['comment_worker'];
        $totalprice = $row['totalprice'];
    }
    ?>
<main>
    <div class="container">
        <div class="row">
            <div class="client">
            <form action="problem_update_check.php" method="POST">

                <label for="problem_id">Problem No: <?php echo $problem_id;?></label>
                <input type="hidden" name="problem_id" value='<?php echo $problem_id;?>' readonly><br>

                <label for="problem_status">Car status: </label>
                <select name="problem_status" id="problem_status" value="<?php echo $problemstatus; ?>">

                <?php
                // We cannot allow admin to set status to 'waiting' or 'declined'
                $sql = "SELECT * FROM carstatus WHERE carstatus_id BETWEEN 2 AND 6";
                $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));
                while ($row = mysqli_fetch_assoc($result)) {
                    $carstatus_id = $row['carstatus_id'];
                    $carstatus_name = $row['carstatus_name'];

                        if ($carstatus_id == $problemstatus) {
                            echo "<option selected name='problem_status' value='{$carstatus_id}'>{$carstatus_name}</option>";
                        }
                        else {
                            echo "<option name='problem_status' value='{$carstatus_id}'>{$carstatus_name}</option>";
                        }
                }
                ?>
                
                </select><br>

                <label for="comment_worker">Add final comment for whole problem</label><br>
                <textarea name="comment_worker" id="comment_worker" cols="30" rows="10"><?php echo $comment;?></textarea><br>

                <label for="totalprice">Update Final Price</label><br>
                <input type="number" name="totalprice" value="<?php echo $totalprice;?>"><br>

                <input type="submit" value="Submit" class="btn btn-success btn-block">
            </form>
            </div>
        </div>
    </div>
</main>

<?php include '../includes/footer.php'; ?>