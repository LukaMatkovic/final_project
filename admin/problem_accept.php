<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';

?>
<?php
if ($_SERVER ['REQUEST_METHOD'] == 'POST') {

    $problem_id = (test_input($_POST['code'])/71-5)/3;

    $sql = "SELECT user_firstname, user_lastname, user_email FROM users INNER JOIN problems ON users.user_id = problems.problem_user 
    WHERE problem_id = '$problem_id'";
    $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

    while ($row = mysqli_fetch_assoc($result)) {
    $user_firstname = $row['user_firstname'];
    $user_lastname = $row['user_lastname'];
    $user_email = $row['user_email'];
    $fullname = $user_firstname." ".$user_lastname;
    }
    //Change status into accepted
    $sql = "UPDATE problems SET problem_status=2 WHERE problem_id = '$problem_id' ";
    $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));
    
    //Send email about this information
    $subject = "Car service - request accepted";
    $message = "Dear ".$fullname.",

                We would like to inform you that your request has been accepted!

                See you in a scheduled time!
                Sincerely, ";

    mail($user_email,$subject,$message);

    header("Location: problem_review.php");
}
else {
    header("Location: ../index.php");
}
?>

<?php include '../includes/footer.php'; ?>