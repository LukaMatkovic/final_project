<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php';
        include '../includes/functions.php';
?>
<?php
if ($_SERVER ['REQUEST_METHOD'] == 'POST') {

    //Text-area validation
    $problem_id = (test_input($_POST['code'])/71-5)/3;
    $comment_worker = test_input($_POST['comment_worker']);
    $error = [];
    if(empty($comment_worker)) {
        array_push($error, "empty");
    }
    if (strlen($comment_worker)<10) {
        array_push($error, "min");
    }
    if (strlen($comment_worker)>250) {
        array_push($error, "max");
    }
    //If textarea is not rightfully fullfilled, return to previous page
    if(!empty($error)) {
        $error = http_build_query($error);
        $params = "error=".urlencode($error);
        $params.= "&com=".urlencode($comment_worker);
        $params.= "&cd=".urlencode($_POST['code']);

        header("Location: ".$_SERVER['HTTP_REFERER']."?".$params."#".$_POST['code']);
        exit();

    }
    //If everything is ok, change status into declined, finalprice=>0 and duration of visit=>0
    else {
        $sql = "SELECT user_firstname, user_lastname, user_email FROM users INNER JOIN problems ON users.user_id = problems.problem_user 
        WHERE problem_id = '$problem_id'";
        $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

        while ($row = mysqli_fetch_assoc($result)) {
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $fullname = $user_firstname." ".$user_lastname;
        }

        $sql = "UPDATE problems SET problem_status=7, totalprice = 0, comment_worker = '$comment_worker' WHERE problem_id = '$problem_id' ";
        $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

        $sql = "SELECT problem_reservation FROM problems WHERE problem_id = '$problem_id' ";
        $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

        while ($row = mysqli_fetch_assoc($result)) {
            $rid = $row['problem_reservation'];

            $sql_update = "UPDATE reservations SET reservations.duration = 0  WHERE reservation_id ='$rid' ";
            $result_update = mysqli_query($connection, $sql_update) OR die('Query failed: '.mysqli_error($connection));
        }
        //Send email about this information

        $subject = "Car service - request refusal";
        $message = "Dear ".$fullname.",

                    We regret to inform you that currently we have to decline your request.".$comment_worker.
                    "
                    Sincerely, ";
        mail($user_email,$subject,$message);

        header("Location: problem_review.php");
        exit();    
    }
}
else {
    header("Location: ../index.php");
    exit();
}
?>

<?php include '../includes/footer.php'; ?>