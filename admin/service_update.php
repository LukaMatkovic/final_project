<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';

?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Update specific service</h2>
            </div>
        </div>
    </div>
</section>
<?php

    $id = 0;
    if(isset($_GET['cd'])) { 
        $id = ((test_input($_GET['cd'])-19)/17+15)/13;}

    $sql_services = "SELECT problem_services.problem_id, services.service_name, problem_services.worker_comment, problem_services.service_finalprice FROM problem_services  INNER JOIN services ON problem_services.service_id=services.service_id WHERE problem_services.ps_id=$id ";
    $result_services = mysqli_query($connection, $sql_services) or die('Query failed: '.mysqli_error($connection));

    while ($row=mysqli_fetch_array($result_services, MYSQLI_BOTH)) {
        $name=$row['service_name'];
        $comment=$row['worker_comment'];
        $price=$row['service_finalprice'];
        $problem_id=$row['problem_id'];
    }
    ?>
<main>
    <div class="container">
        <div class="row">
            <div class="client">

            <form action="service_update_check.php" method="POST">

                <label for="id">Service: <?php echo $name;?></label>

                <input type="hidden" name="id" value='<?php echo $id;?>' readonly><br>
                <input type="hidden" name="problem_id" value='<?php echo $problem_id;?>' readonly><br>

                <label for="worker_comment">Add a Comment to This Service</label><br>
                <textarea name="worker_comment" id="worker_comment" cols="30" rows="10" value=""><?php echo $comment;?></textarea><br>

                <label for="finalprice">Update Service Price</label><br>
                <input type="number" name="finalprice" value="<?php echo $price;?>"><br>

                <input type="submit" value="Submit" class="btn btn-success btn-block">
            </form>
            </div>
        </div>
    </div>
</main>
<?php include '../includes/footer.php'; ?>