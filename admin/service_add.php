<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';

?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add new service for this case</h2>
            </div>
        </div>
    </div>
</section>
<?php

    $problem_id = 0;
    if(isset($_GET['cd'])) { $problem_id = (test_input($_GET['cd'])/71-5)/3;}
    ?>
<main>
    <div class="container">
        <div class="row">
            <div class="client">

            <form action="service_add_check.php" method="POST">

            <label for="problem_id">Problem No: <?php echo $problem_id;?></label>
            <input type="hidden" name="problem_id" value='<?php echo $problem_id;?>' readonly><br>

            <label for="service_id">Service: </label>
            <select name="service_id" id="service_id">

            <?php
                $sql = "SELECT * FROM services";
                $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));
            
                while ($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {
                    $service_id=$row['service_id'];
                    $service_name=$row['service_name'];
                    $service_price=$row['service_price(RSD)'];
                    $service_duration=$row['service_duration(hrs)'];
                    echo "<option name='service_id' value='{$service_id}'>{$service_name} ({$service_price} RSD, {$service_duration}h)</option>";
                }
            ?>

            </select><br>

            <label for="worker_comment">Add specific comment for this service needed</label><br>
            <textarea name="worker_comment" id="worker_comment" cols="30" rows="10" value="" required></textarea><br>
            
            <p>Default price and duration are written above. Fill in required price and duration.</p>

            <label for="price">Price (RSD)</label><br>
            <input type="number" name="price" id="price" value="" required><br>

            <label for="duration">Duration (hours)</label><br>
            <input type="text" name="duration" id="duration" value="" required><br>

            <input type="submit" value="Submit" class="btn btn-success btn-block">
            </form>
        </div>
        </div>
    </div>
</main>


<?php include '../includes/footer.php'; ?>