<?php
if(!defined("ALLOW_CONTACT")){
    die();
}
include_once 'config.php';
include_once 'db.php';

if (isset($_SESSION['user_email'])) {
    $email = $_SESSION['user_email'];
}
?>
        <div class = "col-sm-6 mx-auto" style="color:red;">
<?php
    $error =  $email = $subject = $message = "";
    $msg = [];
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['subject'])) { $subject = $_GET['subject']; }
    if (isset($_GET['message'])) { $message = $_GET['message']; }
    if (isset($_GET['error'])) { $error = $_GET['error']; }

    $error = urldecode($error);
    $errors = explode("&",$error);

    foreach ($errors as $err) {
        array_push($msg, substr($err, 2));
    }
    if (in_array("empty", $msg)) {
        echo "<p>All fields are required!</p>";
    }
    if (in_array("width", $msg)) {
        echo "<p>Minimum characters: <br>
        Subject, Message: 4<br>
        Email: 8</p>";              
    }
    if (in_array("email", $msg)) {
        echo "<p>Incorrect email form!</p>";
    }
?>
        </div>
        <form action="contact_send.php" method="POST" id="form_contact">
            <div class="form-group">
                <label for="email"><strong>Your E-Mail </strong></label>
                <input type="email" name="email" id="email" maxlength=30 minlength=8 placeholder="email@mail.com" value="<?php echo $email; ?>" required="true" class="form-control" >
            </div>
            <div class="form-group">
                <label for="subject"><strong>Subject</strong></label>
                <input type="text" name="subject" id="subject" maxlength="30" minlength=8 placeholder="subject" value="<?php echo $subject; ?>" required="true" class="form-control" >
            </div>
            <div class="form-group">
                <label for="message"><strong>Your Message</strong></label>
                <textarea name="message" id="message" cols="30" rows="10" maxlength=250 minlength=8 required="true" class="form-control" ><?php echo $message; ?></textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="Send" class="btn btn-info form-control">
            </div>
        </form>
        <script src="node_modules/jquery-validation/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="js/form_validation.js"></script>
        