<?php
session_start();
require 'config.php';
require 'db.php';
require 'functions.php';

if ($_SERVER ['REQUEST_METHOD'] == 'POST') {

    $email = test_input($_POST['email']);
    $password = test_input($_POST['password']);

    //Check if inputs are empty

    if (isEmpty([$email,$password])) {
        header("Location: ../index.php?login=empty");
        exit();
    } 
    else {

        $mid_password = SALT1.$password.SALT2;
        $final_password = MD5($mid_password);

        $sql = "SELECT * FROM users WHERE user_email='$email'";
        $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
        
        //Check if email is in users table in database
        if (!mysqli_num_rows($result)>0) {
            header("Location: ../index.php?login=email");
            exit();
        } 
        else {
            
            //Check if inserted password matches with stored one
            while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {

                if ($final_password!=$row['user_password']) {
                    header("Location: ../index.php?login=password");
                    exit();
                } 
                else { 
                    //Check if account is activated
                    if ($row['user_status']!=1) {
                        header("Location: ../index.php?login=activate");
                        exit();
                    }
                    else {

                        $_SESSION['user_id']=$row['user_id'];
                        $_SESSION['user_firstname']=$row['user_firstname'];
                        $_SESSION['user_lastname']=$row['user_lastname'];
                        $_SESSION['user_email']=$row['user_email'];
                        $_SESSION['user_phone']=$row['user_phone'];
                        $_SESSION['user_status']=$row['user_status'];
                        $_SESSION['user_role']=$row['user_role'];
                        $_SESSION['user_token']=$row['user_token'];
                        $_SESSION['_token']=bin2hex(openssl_random_pseudo_bytes(16));

                    //Login as admin
                        if ($_SESSION['user_role']==1) {
                            header("Location: ../admin/start.php");
                            exit();
                        }
                    //Login as user
                        else {
                            header("Location: ../welcome.php");
                            exit();
                        }
                    }
                }
            }
        }
    }
}

else {
    header("Location: ../index.php");
    exit();
}