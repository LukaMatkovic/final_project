<?php
if(!defined("SECRET")){
    die();
}
?>
<?php
    $sql = "SELECT * FROM carbrands";
    $result = mysqli_query($connection, $sql) or die('Query FAILED: '.mysqli_error());

    while ($row = mysqli_fetch_assoc($result)) {
        $brand_id = $row['brand_id'];
        $brand_name = $row['brand_name'];

        if (isset($_GET['car_brand']) && $_GET['car_brand']!="") { 
            $car_brand = $_GET['car_brand']; 

            if ($car_brand == $brand_id) {
                echo "<option selected name='car_brand' value='{$brand_id}'>{$brand_name}</option>";
            }
            else {
                echo "<option name='car_brand' value='{$brand_id}'>{$brand_name}</option>";
            }
        }
        else {
            echo "<option name='car_brand' value='{$brand_id}'>{$brand_name}</option>";

        }
}
?>
