<?php
if(!defined("SECRET")){
    die();
}
?>
<?php
    $h = 8;
    if (isset($_GET['reservation_hour'])) { 

        $reservation_hour = $_GET['reservation_hour'];
        echo "<option selected value='{$reservation_hour}'>{$reservation_hour}h</option>";
        for ($i = 8; $i<16; $i++) {
            $h = $i;
            if ($i<10) {
                $h = "0".$i;
            }
            echo "<option value='{$i}'>{$h}:00</option>";
            echo "<option value='{$i}.5'>{$h}:30</option>";
        }
    }
    else {

        for ($i = 8; $i<16; $i++) {
            $h = $i;
            if ($i<10) {
                $h = "0".$i;
            }
            echo "<option value='{$i}'>{$h}:00</option>";
            echo "<option value='{$i}.5'>{$h}:30</option>";
        }

    }
?>