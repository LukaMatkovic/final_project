<?php
if(!defined("SECRET")){
    die();
}
?>

<script src="js/loginmenu.js"></script>

<?php
    if(!isset($_SESSION['user_role'])) {
?>
    <div id="signup">
        <a class="btn btn-primary" href="signup.php">Sign Up</a></div>
    <div id="login">
        <p>Already registered?</p>
        <button class="btn btn-info" id="lgbtn">Login</button>
    </div>
    <div id="forgot">
        <p>Forgot password?</p>
        <a class="btn btn-danger" href="forgot_password.php" id="lgbtn">Reset</a>
    </div>

    <div id="loginform" class="d-none">
        <form action="includes/login.php" id="login_form" method="POST">
            Email: <br>
            <input type="email" name="email" id="email" placeholder="Enter your email here" required><br>
            Password: <br>
            <input type="password" name="password" id="password" placeholder="Your password goes here" required><br>
            <input type="submit"  class="btn btn-primary" name="submit" value="Login" id="finallogin"><br>
        </form>          
    </div>
<?php
    }
?>

