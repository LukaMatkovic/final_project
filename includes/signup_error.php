<?php
if(!defined("SECRET")){
    die();
}
?>
<?php
$error = $firstname = $lastname = $email = $phone = $signup = "";
                $msg = [];
                if (isset($_GET['firstname'])) { $firstname = $_GET['firstname']; }
                if (isset($_GET['lastname'])) { $lastname = $_GET['lastname']; }
                if (isset($_GET['email'])) { $email = $_GET['email']; }
                if (isset($_GET['phone'])) { $phone = $_GET['phone']; }
                if (isset($_GET['error'])) { $error = $_GET['error']; }

                //$error is an array turned to string (in register.php) that contains error names of all inputs.
                //First we have to decode it, return as an array(explode) and then show all the errors in $msg.

                $error = urldecode($error);
                $errors = explode("&",$error);

                foreach ($errors as $err) {
                    array_push($msg, substr($err, 2));
                }
            
                if (in_array("empty", $msg)) {
                    echo "<p>All fields are required!</p>";
                }
                if (in_array("min", $msg)) {
                    echo "<p>Min length: <br>
                    firstname, lastname: 2 characters<br>
                    email, phone, password: 8 characters
                    </p>";              
                }
                if (in_array("max", $msg)) {
                    echo "<p>Max length: <br>
                    firstname, lastname, email, password: 30 characters <br>
                    phone: 15 characters
                    </p>";              
                }
                if (in_array("invalid", $msg)) {
                    echo "<p>Names can only contain letters! <br>
                    Phone number only contains numbers!</p>";
                }
                if (in_array("email", $msg)) {
                    echo "<p>Incorrect email form!</p>";
                }
                if (in_array("usedmail", $msg)) {
                    echo "<p>User with this address is already registered!</p>";
                }
                if (in_array("password", $msg)) {
                    echo "<p>Confirmed password is not the same as the first password!</p>";
                }
?>