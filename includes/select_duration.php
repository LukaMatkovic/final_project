<?php
if(!defined("SECRET")){
    die();
}
?>
<?php
    if (isset($_GET['duration'])) { 
        $duration = $_GET['duration'];
        
        for($i = 0.5; $i<=4; $i+=0.5) {
            if ($i == $duration) {
                echo "<option selected value='{$i}'>{$i} h</option>";
            }
            else {
                echo "<option value='{$i}'>{$i} h</option>";
            }
        }
    }    
    else {
        for($i = 0.5; $i<=4; $i+=0.5) {
            echo "<option value='{$i}'>{$i} h</option>";
        }
    }
?>