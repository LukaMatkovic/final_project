<?php 
include 'head.php'; 
if(!defined("ALLOW_HEADER")){
    die();
}
?>
            <nav class="navbar navbar-expand-sm navbar-dark bg-dark fixed-top">
                <div class="container">

                    <a class="navbar-brand" href="index.php">CarService</a>

                    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse text-center" id="navbarCollapse">

                        <ul class="navbar-nav ml-auto">
                            <?php
                            if(isset($_SESSION['user_role']) && $_SESSION['user_role']==1) {
                                //Admin-only page
                                echo "<li class='nav-item'><a class='btn btn-info' href='admin/start.php'>ADMIN</a></li>";}
                            ?>
                                <li class="nav-item"><a class="nav-link" href='index.php'>Home</a></li>
                            <?php 
                            if(isset($_SESSION['user_role']) && $_SESSION['user_role']!=1) {
                                //User-only page
                                echo "<li class='nav-item'><a class='nav-link' href='welcome.php'>User</a></li>";} 
                            ?>
                                <li class="nav-item"><a class="nav-link" href='index.php#services'>Services</a></li>
                                <li class="nav-item"><a class="nav-link" href='pricelist.php'>Pricelist</a></li>
                                <li class="nav-item"><a class="nav-link" href='aboutus.php'>About Us</a></li>
                                <li class="nav-item"><a class="nav-link" href='index.php#workers'>Our Team</a></li>
                                <li class="nav-item"><a class="nav-link" href='index.php#contact'>Contact</a></li>

                            <?php
                            if(isset($_SESSION['user_role'])) {
                                    $tk = $_SESSION['_token'];
                                echo "<li class='nav-item'><a class='btn btn-danger' id='logout' href='logout.php?logout=".$tk."'>LOGOUT</a></li>"; 
                            }
                            ?>

                        </ul>

                    </div>
                </div>
            </nav>
            </header>

