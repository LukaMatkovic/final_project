<?php
if(!defined("ALLOW_HEADER")){
    die();
}
?>
<footer class="bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <p class="mr-auto">Copyright &copy; by Luka Matkovic & Mladen Petkoski, 2018</p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
