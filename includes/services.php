<?php
    include_once 'config.php';
    include_once 'db.php';
if(!defined("SECRET")){
    die();
}
?>
            <div class="col text-center p-5">
                <h2 class="dislpay-4">Our services</h2>
            </div>
<script src="js/scroll.js"></script>
<?php

    $sql = "SELECT * FROM services";
    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $name = $row['service_name'];
        $description = $row['service_description'];
        $price = $row['service_price(RSD)'];
        $duration = $row['service_duration(hrs)'];
        $photo = substr($row['service_photo'], 3); 
        ?>

                <div class="row scroll-animations">
                    <div class="col-md-6 animated">
                        <img src="<?php echo $photo; ?>" alt="<?php echo $name; ?>" class="img-fluid mb-3 img-thumbnail">
                    </div>
                    <div class="col-md-6 services-text animated">
                        <h3><?php echo $name; ?></h3>
                        <p><?php echo $description; ?></p>
                        <p><strong>Price: </strong><?php echo $price; ?> RSD</p>
                        <p><strong>Duration: </strong><?php echo $duration; ?>h</p>
                    </div>
                </div>


        <?php
    }

?>
