<?php

include_once 'config.php';
include_once 'db.php';

function test_input($data) {
    global $connection;
    $data = trim($data); //strips whitespace (or other characters) from the beginning and end of a string
    $data = stripslashes($data);//removes backslashes added by the addslashes() function.
    $data = htmlspecialchars($data);//converts some predefined characters to HTML entities
    $data = mysqli_real_escape_string($connection, $data); //against SQL injection
    return $data;
}
function isEmpty(array $datas) {
    $err = [];
    foreach ($datas as $data) {
        if(empty($data)) {
            array_push($err, $data);
        }
    }
    if(empty($err)) {
        return false;
    }
    else {
        return true;
    }
}
function minLength(array $datas) {
    $err = [];
    foreach ($datas as $data) {
        if (is_array($data)) {
            if(strlen($data[0])<$data[1]) {
                array_push($err, $data[0]);
            }
        }
    }
    if(empty($err)) {
        return false;
    }
    else {
        return true;
    }
}
function maxLength(array $datas) {
    $err = [];
    foreach ($datas as $data) {
        if (is_array($data)) {
            if(strlen($data[0])>$data[1]) {
                array_push($err, $data[0]);
            }
        }
    }
    if(empty($err)) {
        return false;
    }
    else {
        return true;
    }
}
function generateNewString($len = 10) {
    $token = "poiuztrewqasdfghjklmnbvcxy1234567890";
    $token = str_shuffle($token);
    $token = substr($token, 0, $len);

    return $token;
}

function redirectToLoginPage() {
    header('Location: index.php');
    exit();
}

?>