<?php

$login = "";

if (isset($_GET['login'])) { 
    echo "<div class='col alert alert-danger'>";
    $login = $_GET['login']; 
    if ($login =='empty') {
        echo "<p>Please, fill out email and password!</p>";
    }
    elseif ($login =='email') {
        echo "<p>You have not been registered yet! Please, sign up.</p>";
        echo "<div><a href=\"signup.php\">Sign Up</a></div>";
    }
    elseif ($login =='password') {
        echo "<p>Incorrect password!</p>";
    }
    elseif ($login =='activate') {
        echo "<p>Your profile has not been activated yet!<br>
                Please, go to your email and click the link we've sent you to activate your profile.</p>";
    }  
    else {
        echo "<p>Error. Try again!</p>";
    } 
    echo "</div>";
}

?>