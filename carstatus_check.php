<?php 
    include 'includes/config.php';
    include 'includes/db.php';
    include 'includes/functions.php';
    session_start(); 
?>

<?php
    if (!isset($_SESSION['user_id']) || $_SESSION['user_role']!=2) {
        header("Location: index.php");
        exit();
    }
    $car_user = $_SESSION['user_id'];
    $user = $_SESSION['user_firstname']." ".$_SESSION['user_lastname'];

    $car_id = 0;
    $car_plate = "";
    $carstatus = "";
    $err = "";
    $msg = "";


    if(isset($_POST['carplate'])) {
        $car_plate = test_input($_POST['carplate']);
    }
    //Check if insert is valid
    if (empty($car_plate)) {
        $err = "Please enter your carplate!";
    }
    elseif (!preg_match("/^[A-Z0-9]*$/", $car_plate)) {
        $err = "Only capital letters and numbers are allowed (no dashes and spaces)!";
    }
    elseif (strlen($car_plate)<5 || strlen($car_plate)>8) {
        $err = "Carplates contain 5-8 characters (capital letters and numbers only)!";
    }
    else {
        $err = "";
    }
    //Check if carplates match with user and carstatus
    $sql = "SELECT car_id FROM cars WHERE car_user = '$car_user' AND car_plate = '$car_plate'";
    $result = mysqli_query($connection, $sql) or die("Query failed: ".mysqli_error($connection));

    if(mysqli_num_rows($result)>0) {

        while($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $car_id = $row[0];
        }

        $sql = "SELECT carstatus.carstatus_name FROM problems 
                INNER JOIN carstatus ON problems.problem_status = carstatus.carstatus_id 
                WHERE problem_car = '$car_id'";
        $result = mysqli_query($connection, $sql) or die("Query failed: ".mysqli_error($connection));

        if(mysqli_num_rows($result)>0) {

            while($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $carstatus = $row[0];
            }
        }
        if ($carstatus == "resolved") {
            $msg = "The last service is resolved, your car is returned. <br> If you have another problem, please fill in problem report.";
        }
        elseif ($carstatus == "declined") {
            $msg = "Your last request has been declined, please report a problem again!";
        }
    
    }
    else {
        $msg = " do not match with yours.";
    }
    //Generate JSON    
    $data['user'] = $user;
    $data['carplate'] = $car_plate;
    $data['carstatus'] = $carstatus;
    $data['error'] = $err;
    $data['message'] = $msg;

    header('Content-type: text/JSON');
    echo json_encode($data,JSON_PRETTY_PRINT);

?>

