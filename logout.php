<?php
    session_start();

    if ((!isset($_GET['logout'])) || ($_GET['logout'] != $_SESSION['_token'])) {

        header("Location: index.php");
        exit();
    }
    else {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }
?>
