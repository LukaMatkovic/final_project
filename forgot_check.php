<?php
    include 'includes/config.php';
    include "includes/db.php";
    include "includes/functions.php";
    include "includes/head";
    include "includes/header.php";

         
    

    if (isset($_POST['email'])) {

        $email = test_input($_POST['email']);

        $sql = "SELECT * FROM users WHERE user_email='$email'";
        $result = mysqli_query($connection, $sql);
        if (mysqli_num_rows($result) > 0) {

            $token = generateNewString();
            $sqlResTok = "UPDATE users SET reset_token='$token', 
            reset_token_expire=DATE_ADD(NOW(), INTERVAL 30 MINUTE)
            WHERE user_email='$email'";
            mysqli_query($connection,$sqlResTok);
              //Sending email for confirmation
        $message = "<h1>Confirm Your Registration</h1><br>
        Please, click on this link to reset your Password!
        http://mplmproject.com/reset_password.php?email=$email&token=$token";
        $headers = "From: MPLM <no-reply@mplmproject.com> \r\n";
        mail($email,"Confirmation Mail",$message,$headers);
           
    
    
            
            
}}
?>

<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">
        
            <p>Please, go to your email account and click on the link in the message we sent you so you can reset your password!</p>
        </div>
    </div>
</main>
<?php include 'includes/footer.php'?>