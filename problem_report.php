<?php 
    include 'includes/config.php';
    include 'includes/header.php';
    include 'includes/db.php';

    if (!isset($_SESSION['user_id']) || $_SESSION['user_role']!=2) {
        header("Location: index.php");
        exit();
    }
?>
    <div class="jumbotron">
        <div class = "col-sm-6 mx-auto" style="color:red;">
            <?php include 'includes/problem_report_error.php'; ?>
        </div>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="node_modules/jquery-validation/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="js/problem_report_validation.js"></script>

        <div id="formWindow" class="row">
            <div class="col-sm-4 mx-auto">
                <form action="problem_report_check.php" method="POST" id="form_problemreport">
                    <div class="form-group">
                        <label for="car_plate">Carplate: </label><br>
                        <input type="text" class="form-control" name="car_plate" minlength=5 maxlength=8 required="true" value="<?php echo $car_plate; ?>"><br>
                    </div>
                    <div class="form-group">
                        <label for="car_brand">Brand: </label><br>
                        <select name="car_brand" id="car_brand" class="form-control text-center" required="true">
                            <option selected value="">--Choose--</option>
                            <?php include 'includes/select_carbrands.php'; ?>
                        </select><br>
                    </div>
                    <div class="form-group">
                        <label for="car_name">Carname: </label><br>
                        <input type="text" class="form-control" name="car_name" minlength=2 maxlength=16 required="true" value="<?php echo $car_name; ?>"><br>
                    </div>
                    <div class="form-group">
                        <label for="comment_user">Explain Your Problem: </label><br>
                        <textarea name="comment_user" id="comment_user" class="form-control" minlength=10 maxlength=250 required="true"  cols="30" rows="10"><?php echo $comment_user; ?></textarea><br>
                    </div>
                    <div class="form-group">
                        <label for="service_id">Services needed: <br>
                        <?php include 'includes/checkbox_services.php'; ?>
                        </label>
                    </div>
            </div>

            <div class="col-sm-3 text-center mx-auto">
                <?php include 'includes/reservations.html'; ?>
            </div>

            <div class="col-sm-5 mx-auto">
                    <p><strong>Reservation</strong></p>
                    <div class="form-group">
                        <label for="day"><strong>Date</strong></label><br>
                        <p>Select the day you want to come, and then check for free appointments!</p>
                        <input type="text" name="day" id="datepicker"  required="true" value="<?php echo $reservation_day; ?>" readonly><br>
                        <p>Select the time you want to come and duration of your visit.</p>
                        <p id="reservation_error"></p>
                    </div>

                    <div class="form-group">
                        <label for="time"><strong>Time: </strong></label>
                        <select name="time" id="time" required="true">
                            <option selected value="">--Choose--</option>
                            <?php include 'includes/select_time.php'; ?>
                        </select><br>
                        <p>Please, choose duration according to time needed for services you need.<br>
                        Maximum reservation time: 4 hours. <br>
                        You can check as many services as you wish, no matter if time exceeds 4 hours.</p>
                    </div>

                    <div class="form-group">
                        <label for="duration"><strong>Duration: </strong></label>
                        <select name="duration" id="duration" required="true">
                            <option selected value="">--Choose--</option>
                            <?php include 'includes/select_duration.php'; ?>
                        </select><br>
                    </div>

                    <input type="submit" id="final" name="submit" class="btn btn-primary" value="Report">
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">

    function loadCSS(filename) {
    
    var file = document.createElement("link");
    file.setAttribute("rel", "stylesheet");
    file.setAttribute("type", "text/css");
    file.setAttribute("href", filename);

    document.head.appendChild(file);
    }

    loadCSS("//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
    loadCSS("/resources/demos/style.css");

</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/datepicker.js"></script>
<script src="js/reservation_check.js"></script>

<?php include 'includes/footer.php'; ?>