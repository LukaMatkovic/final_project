<?php   
    include 'includes/config.php';
    include 'includes/header.php'; 
    include 'includes/loginmenu.php';

    if (!isset($_SESSION['user_id']) || $_SESSION['user_role']!=2) {
        header("Location: index.php");
        exit();
    }
    
?>
<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">

        <?php echo "Welcome, {$_SESSION['user_firstname']} {$_SESSION['user_lastname']}."; ?>

            <div class="col-md-3">
                <div class="row mr-4">
                    <a class="btn btn-primary" href="problem_report.php">REPORT A PROBLEM</a><br>
                </div>
                <div class="row m4-4">
                    <a class="btn btn-primary" href="carstatus.php">CHECK STATUS</a>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include 'includes/footer.php'; ?>
