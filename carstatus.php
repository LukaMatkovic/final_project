<?php 
include 'includes/config.php';
include 'includes/header.php'; ?>

<?php
    if (!isset($_SESSION['user_id']) || $_SESSION['user_role']!=2) {
        header("Location: index.php");
        exit();
    }
    $user_id = $_SESSION['user_id'];
?>

<script src="js/carstatus.js"></script>

<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">
            <div id="formWindow" class="row">
                <div class="col-sm-12 mx-auto text-center">
                    <form action="carstatus_check.php" id="form_checkstatus" method="POST">
                
                        <label for="carplate">Enter your carplate (only numbers and capital letters, no dashes)</label><br>
                        <input type="text" name="carplate" id="carplate" placeholder="AA000AA"><br>

                        <input type="submit" value="Check" class="btn btn-info">
                    </form>
                </div>
            </div>
            <p id="here"></p>
        </div>
    </div>
</main>

<?php    include 'includes/footer.php'; ?>
