$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'error',
        highlight: function(element){
            $(element)
            .closest('.form-group')
            .addClass('has-error');
        },
        unhighlight: function(element){
            $(element)
            .closest('.form-group')
            .removeClass('has-error');
        }
    })
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var check = false;
            return this.optional(element) || regexp.test(value);
        },
        "Please check your input."
    );
$("#form_register, #form_contact").validate({
        errorClass: "error",
        validClass: "ok",
        rules: {
            firstname: {
                minlength: 2,
                maxlength: 30,
                required: true,
                regex: /^[a-zA-Z ]*$/
            },
            lastname: {
                minlength: 2,
                maxlength: 30,
                required: true,
                regex: /^[a-zA-Z ]*$/
            },
            email: {
                email: true,
                minlength: 8,
                maxlength: 30,
                required: true
            },
            phone: {
                regex: /^[0-9+]*$/,
                required: true,
                minlength: 8,
                maxlength: 15
            },
            password:  {
                required: true,
                minlength:8,
                maxlength: 30,
                regex: /^[a-zA-Z0-9]*$/
            },
            confirm:  {
                required: true,
                minlength:8,
                maxlength: 30,
                equalTo: "#password"
            },
            subject: {
                required: true,
                minlength:8,
                maxlength: 30
            },
            message: {
                required: true,
                minlength:8,
                maxlength: 250
            }
        },
        messages: {
            firstname: {
                minlength: "At least 2 characters required!",
                maxlength: "Max 30 characters allowed!",
                required: "Please, enter your firstname!",
                regex: "Names have to contain only letters and spaces!"
            },
            lastname: {
                minlength: "At least 2 characters required!",
                maxlength: "Max 30 characters allowed!",
                required: "Please, enter your lastname!",
                regex: "Names have to contain only letters and spaces!"
            },
            email: {
                email: "Please, fill out correct email format!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
                required: "Please, enter your email!"
            },
            phone: {
                regex: "Only numbers and + allowed (no spaces, no /,(),-)",
                required: "Please, enter your phone!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 15 characters allowed!"
            },
            password:  {
                required: "Please, provide a password!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
                regex: "Password can only contain letters and numbers!"
            },
            confirm:  {
                required: "Please, confirm your password!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
                equalTo: "Please, enter the same password as above!"
            },
            subject: {
                required: "Please, enter the subject of your message!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
            },
            message: {
                required: "Please, enter the content of your message!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 250 characters allowed!"
            }        
        }
    });
    $("#login_form" ).validate({
        errorClass: "error",
        validClass: "ok",
        rules: {
            email: {
                email: true,
                minlength: 8,
                maxlength: 30,
                required: true
            },
            password:  {
                required: true,
                minlength:8,
                maxlength: 30,
                regex: /^[a-zA-Z0-9]*$/
            }
        },
        messages: {
            email: {
                email: "Please, fill out correct email format!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
                required: "Please, enter your email!"
            },
            password:  {
                required: "Please, provide a password!",
                minlength: "At least 8 characters required!",
                maxlength: "Max 30 characters allowed!",
                regex: "Password can only contain letters and numbers!"
            }      
        }
    });

});
