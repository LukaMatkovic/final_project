var user;
var carstatus;
var carplate;
var final;
var result;
var error;
var msg;

$(document).ready(function(){

    $("#form_checkstatus").submit(function(event) {
        //turn off default function
    event.preventDefault();
    var url = $("#form_checkstatus").attr('action');
    var type = $("#form_checkstatus").attr('method');
    var data = {};
    var name = $("#carplate").attr('name');
    var value = $("#carplate").val();

    //ajax call to carstatus_check.php
    $.ajax({
        url: url,
        type: type,
        data: {'carplate': value},
        dataType: "JSON",

        success: function(data) {
            console.log(data);
        },

        complete: function(data) {
                result = $.parseJSON(data.responseText);
                user = result.user;
                carstatus = result.carstatus;
                carplate = result.carplate;

                //check if user matches with inserted carplate
                if (result.error != "") {
                    error = result.error;
                    final = error;
                }
                else if (result.carstatus == "") {
                    msg = result.message;
                    final = "Dear "+user+", <br> Carplates you inserted (' "+carplate+" ') "+msg;
                }
                //check if status is resolved or declined
                else if  (result.carstatus == "resolved" || result.carstatus == "declined") {
                    msg = result.message;
                    final = "Dear "+user+", <br> For your car with carplates "+carplate+",<br> "+msg;
                }
                //currently in service
                else {
                    final = "Dear "+user+", <br>Your car with carplates "+carplate+" is currently in <strong>"+carstatus+"</strong> status.";
                }
                $("#here").html(final);
        },

        error: function() {
            final = "Something went wrong.";
            $("#here").html(final);

        }
    });
    })

});
